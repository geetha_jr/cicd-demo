package com.beehyv.gitlabcicddemo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
  
  @Value("${app.name}")
	private String appName;
  
  @RequestMapping(method = RequestMethod.GET, value = "/")
  public String sayHello() {
    return "YaHallo! Welcome to " + appName;
  }

}
